/* This file is part of sapwood
 *
 * Copyright (C) 2010  Sven Herzberg
 *
 * This work is provided "as is"; redistribution and modification
 * in whole or in part, in any medium, physical or electronic is
 * permitted without restriction.
 *
 * This work is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * In no event shall the authors or contributors be liable for any
 * direct, indirect, incidental, special, exemplary, or consequential
 * damages (including, but not limited to, procurement of substitute
 * goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether
 * in contract, strict liability, or tort (including negligence or
 * otherwise) arising in any way out of the use of this software, even
 * if advised of the possibility of such damage.
 */

#include "test-sapwood.h"

#include "test-framework.h"
#include "theme-pixbuf.h"

static void
test_render_clipped (void)
{
  GdkRectangle  clip_rect = {100, 100, 100, 100};
  GdkDrawable * drawable;
  ThemePixbuf * pixbuf;
  GdkBitmap   * mask;
  GdkPixbuf   * expected;
  GdkPixbuf   * result;
  GError      * error = NULL;

  pixbuf = theme_pixbuf_new ();
  theme_pixbuf_set_filename (pixbuf,
                             TOP_SRCDIR G_DIR_SEPARATOR_S
                             "tests" G_DIR_SEPARATOR_S
                             "test-source-alpha75.png");
  theme_pixbuf_set_border   (pixbuf, 16, 16, 16, 16);
  g_test_queue_destroy ((GFreeFunc) theme_pixbuf_unref, pixbuf);

  drawable = create_pixmap (200, 200);

  mask = gdk_pixmap_new (NULL, 200, 200, 1);
  g_test_queue_unref (mask);

  theme_pixbuf_render (pixbuf,
                       GTK_TYPE_BUTTON,
                       drawable,
                       mask,
                       &clip_rect,
                       COMPONENT_ALL,
                       FALSE, /* FIXME: test with TRUE as well */
                       0, 0,
                       200, 200);

  result = gdk_pixbuf_get_from_drawable (NULL, drawable,
                                         gdk_drawable_get_colormap (drawable),
                                         100, 100, 0, 0, 100, 100);
  g_test_queue_unref (result);

  expected = gdk_pixbuf_new_from_file (TOP_SRCDIR G_DIR_SEPARATOR_S "tests" G_DIR_SEPARATOR_S "theme-pixbuf-render-clipped.png", &error);
  g_assert_no_error (error);
  g_test_queue_unref (expected);

  assert_cmp_pixbuf (result, ==, expected);
}

static void
test_render_cropped (void)
{
  GdkRectangle  clip_rect = {0, 0, 50, 50};
  GdkDrawable * drawable;
  ThemePixbuf * pixbuf;
  GdkPixbuf   * expected;
  GdkPixbuf   * result;
  GError      * error = NULL;

  pixbuf = theme_pixbuf_new ();
  theme_pixbuf_set_filename (pixbuf,
                             TOP_SRCDIR G_DIR_SEPARATOR_S
                             "tests" G_DIR_SEPARATOR_S
                             "test-source-alpha75.png");
  theme_pixbuf_set_border   (pixbuf, 16, 16, 16, 16);
  g_test_queue_destroy ((GFreeFunc) theme_pixbuf_unref, pixbuf);

  drawable = create_pixmap (200, 200);

  theme_pixbuf_render (pixbuf,
                       GTK_TYPE_BUTTON,
                       drawable,
                       NULL, /* cropping is only used when mask == NULL */
                       &clip_rect,
                       COMPONENT_ALL,
                       FALSE, /* FIXME: test with TRUE as well */
                       0, 0,
                       50, 50);

  result = gdk_pixbuf_get_from_drawable (NULL, drawable,
                                         gdk_drawable_get_colormap (drawable),
                                         0, 0, 0, 0, 50, 50);
  g_test_queue_unref (result);

  expected = gdk_pixbuf_new_from_file (TOP_SRCDIR G_DIR_SEPARATOR_S "tests" G_DIR_SEPARATOR_S "theme-pixbuf-render-cropped.png", &error);
  g_assert_no_error (error);
  g_test_queue_unref (expected);

  assert_cmp_pixbuf (result, ==, expected);
}

void
test_theme_pixbuf (void)
{
  g_test_add_func ("/sapwood/theme-pixbuf/render-clipped", test_render_clipped);
  g_test_add_func ("/sapwood/theme-pixbuf/render-cropped", test_render_cropped);
}

/* vim:set et sw=2 cino=t0,f0,(0,{s,>2s,n-1s,^-1s,e2s: */
