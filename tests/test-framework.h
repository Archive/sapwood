/* This file is part of sapwood
 *
 * Copyright (C) 2010  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef TEST_FRAMEWORK_H
#define TEST_FRAMEWORK_H

#include <stdint.h>         /* uintptr_t */
#include <gdk/gdk.h>

G_BEGIN_DECLS

GdkDrawable* create_pixmap (int width,
                            int height);

#define assert_cmp_pixbuf(a,op,b) \
  G_STMT_START { \
    if (a && b) \
      { \
        int cmp_x, cmp_y; \
        guchar* pixels_##a; \
        guchar* pixels_##b; \
        g_assert_cmpint (gdk_pixbuf_get_n_channels (a), \
                         ==, \
                         gdk_pixbuf_get_n_channels (b)); \
        g_assert_cmpint (gdk_pixbuf_get_bits_per_sample (a), \
                         ==, \
                         gdk_pixbuf_get_bits_per_sample (b)); \
        g_assert_cmpint (gdk_pixbuf_get_colorspace (a), \
                         ==, \
                         gdk_pixbuf_get_colorspace (b)); \
        g_assert_cmpint (gdk_pixbuf_get_has_alpha (a), \
                         ==, \
                         gdk_pixbuf_get_has_alpha (b)); \
        g_assert_cmpint (gdk_pixbuf_get_height (a), \
                         ==, \
                         gdk_pixbuf_get_height (b)); \
        g_assert_cmpint (gdk_pixbuf_get_width (a), \
                         ==, \
                         gdk_pixbuf_get_width (b)); \
        pixels_##a = gdk_pixbuf_get_pixels (a); \
        pixels_##b = gdk_pixbuf_get_pixels (b); \
        for (cmp_y = 0; cmp_y < gdk_pixbuf_get_height (a); cmp_y++) \
          { \
            for (cmp_x = 0; cmp_x < gdk_pixbuf_get_width (a); cmp_x++) \
              { \
                guchar* pixel_##a = pixels_##a + cmp_y * gdk_pixbuf_get_rowstride (a) + cmp_x * gdk_pixbuf_get_n_channels (a); \
                guchar* pixel_##b = pixels_##b + cmp_y * gdk_pixbuf_get_rowstride (b) + cmp_x * gdk_pixbuf_get_n_channels (b); \
                guint value_##a = (pixel_##a[0] << 16) + (pixel_##a[1] << 8) + pixel_##a[2]; \
                guint value_##b = (pixel_##b[0] << 16) + (pixel_##b[1] << 8) + pixel_##b[2]; \
                if (gdk_pixbuf_get_has_alpha (a)) \
                  { \
                    value_##a <<= 8; value_##a += pixel_##a[3]; \
                    value_##b <<= 8; value_##b += pixel_##b[3]; \
                  } \
                if (value_##a op value_##b) ; else \
                  { \
                    gchar const* name_one = G_STRINGIFY (a) ".png"; \
                    gchar const* name_two = G_STRINGIFY (b) ".png"; \
                    gdk_pixbuf_save (a, name_one, "png", NULL, NULL); \
                    gdk_pixbuf_save (b, name_two, "png", NULL, NULL); \
                    g_message ("pixels at (%d;%d) differ: output images at \"%s\" and \"%s\"", \
                               cmp_x, cmp_y, name_one, name_two); \
                    g_assert_cmphex (value_##a, op, value_##b); \
                  } \
              } \
          } \
      } \
    else \
      { \
        g_assert_cmpuint ((uintptr_t)a, ==, (uintptr_t)b); \
      } \
  } G_STMT_END

G_END_DECLS

#endif /* !TEST_FRAMEWORK_H */

/* vim:set et sw=2 cino=t0,f0,(0,{s,>2s,n-1s,^-1s,e2s: */
