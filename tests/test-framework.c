/* This file is part of sapwood
 *
 * Copyright (C) 2010  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "test-framework.h"

GdkPixmap*
create_pixmap (int  width,
               int  height)
{
  GdkColormap* colormap;
  GdkDrawable* drawable;
  cairo_t    * cr;

  colormap = gdk_screen_get_system_colormap (gdk_screen_get_default ());
  g_assert (colormap);

  drawable = gdk_pixmap_new (NULL, 200, 200, gdk_colormap_get_visual (colormap)->depth);
  g_test_queue_unref (drawable);
  gdk_drawable_set_colormap (drawable, colormap);

  cr = gdk_cairo_create (drawable);
  cairo_set_source_rgb (cr, 1.0, 1.0, 1.0);
  cairo_paint (cr);
  cairo_destroy (cr);

  return drawable;
}

/* vim:set et sw=2 cino=t0,f0,(0,{s,>2s,n-1s,^-1s,e2s: */
