/* This file is part of sapwood
 *
 * Copyright (C) 2010  Sven Herzberg
 *
 * This work is provided "as is"; redistribution and modification
 * in whole or in part, in any medium, physical or electronic is
 * permitted without restriction.
 *
 * This work is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * In no event shall the authors or contributors be liable for any
 * direct, indirect, incidental, special, exemplary, or consequential
 * damages (including, but not limited to, procurement of substitute
 * goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether
 * in contract, strict liability, or tort (including negligence or
 * otherwise) arising in any way out of the use of this software, even
 * if advised of the possibility of such damage.
 */

#include "test-sapwood.h"

#include <errno.h>          /* errno */
#include <stdlib.h>         /* realpath() */
#include <gdk/gdkx.h>       /* GDK_DISPLAY_XDISPLAY() */
#include <sapwood-pixmap.h>

#include "sapwood-cairo.h"
#include "test-framework.h"

static void
test_larger (void)
{
  SapwoodPixmap* pixmap;
  SapwoodRect    rects[9];
  GdkDrawable  * drawable = NULL;
  GdkPixbuf    * result;
  GdkPixbuf    * expected;
  cairo_t      * cr;
  GError       * error = NULL;
  char           abspath[PATH_MAX + 1];
  int            code;
  int            i;

  if (!realpath (TOP_SRCDIR G_DIR_SEPARATOR_S "tests" G_DIR_SEPARATOR_S "cropping-label1.png", abspath))
    {
      g_warning ("error in realpath(): \"%s\" causes %s",
                 abspath,
                 g_strerror (errno));
    }
  pixmap = sapwood_pixmap_get_for_file (abspath,
                                        16, 16, 16, 16,
                                        &error);
  g_test_queue_destroy ((GFreeFunc) sapwood_pixmap_free, pixmap);
  g_assert_no_error (error);

  for (i = 0; i < G_N_ELEMENTS (rects); i++)
    {
      int col = i % 3;
      int row = i / 3;

      sapwood_pixmap_get_pixmap (pixmap, col, row,
                                 &rects[i].source, &rects[i].mask);

      rects[i].dest.x = col < 1 ? 0 : col < 2 ? 16 : 200 - 16;
      rects[i].dest.y = row < 1 ? 0 : row < 2 ? 16 : 200 - 16;
      rects[i].dest.width =  col == 1 ? 200 - 2 * 16 : 16;
      rects[i].dest.height = row == 1 ? 200 - 2 * 16 : 16;
    }

  drawable = create_pixmap (200, 200);

  gdk_error_trap_push ();

  cr = gdk_cairo_create (drawable);
  g_test_queue_destroy ((GFreeFunc) cairo_destroy, cr);
  sapwood_pixmap_render_rects (pixmap,
                               GTK_TYPE_BUTTON,
                               cr,
                               0, 0,
                               200, 200,
                               NULL,
                               0, 0,
                               FALSE,
                               NULL,
                               G_N_ELEMENTS (rects), rects);

  gdk_flush ();
  code = gdk_error_trap_pop ();
  if (code)
    {
      XGetErrorText (GDK_DISPLAY_XDISPLAY (gdk_display_get_default ()),
                     code,
                     abspath,
                     sizeof (abspath)); /* FIXME: check return code once we know _what_ it returns */
      g_warning ("X11 error detected: %s (%d)", abspath, code);
    }

  result = gdk_pixbuf_get_from_drawable (NULL, drawable,
                                         gdk_drawable_get_colormap (drawable),
                                         0, 0, 0, 0,
                                         200, 200);
  g_test_queue_unref (result);

  expected = gdk_pixbuf_new_from_file (TOP_SRCDIR G_DIR_SEPARATOR_S "tests" G_DIR_SEPARATOR_S "test-larger.png", &error);
  g_assert_no_error (error);
  g_test_queue_unref (expected);

  assert_cmp_pixbuf (result, ==, expected);
}

static void
test_larger_masked (void)
{
  cairo_surface_t* mask_surface;
  SapwoodPixmap  * pixmap;
  SapwoodRect      rects[9];
  GdkDrawable    * drawable = NULL;
  GdkPixbuf      * result;
  GdkPixbuf      * expected;
  GdkBitmap      * mask;
  cairo_t        * cr;
  GError         * error = NULL;
  char             abspath[PATH_MAX + 1];
  int              code;
  int              i;

  if (!realpath (TOP_SRCDIR G_DIR_SEPARATOR_S "tests" G_DIR_SEPARATOR_S "test-source-alpha75.png", abspath))
    {
      g_warning ("error in realpath(): \"%s\" causes %s",
                 abspath,
                 g_strerror (errno));
    }
  pixmap = sapwood_pixmap_get_for_file (abspath,
                                        16, 16, 16, 16,
                                        &error);
  g_test_queue_destroy ((GFreeFunc) sapwood_pixmap_free, pixmap);
  g_assert_no_error (error);

  for (i = 0; i < G_N_ELEMENTS (rects); i++)
    {
      int col = i % 3;
      int row = i / 3;

      sapwood_pixmap_get_pixmap (pixmap, col, row,
                                 &rects[i].source, &rects[i].mask);

      rects[i].dest.x = col < 1 ? 0 : col < 2 ? 16 : 200 - 16;
      rects[i].dest.y = row < 1 ? 0 : row < 2 ? 16 : 200 - 16;
      rects[i].dest.width =  col == 1 ? 200 - 2 * 16 : 16;
      rects[i].dest.height = row == 1 ? 200 - 2 * 16 : 16;
    }

  drawable = create_pixmap (200, 200);

  mask = gdk_pixmap_new (NULL, 200, 200, 1);
  g_test_queue_unref (mask);
  mask_surface = sapwood_create_surface (mask);
  g_test_queue_destroy ((GFreeFunc) cairo_surface_destroy, mask_surface);

  gdk_error_trap_push ();

  cr = gdk_cairo_create (drawable);
  g_test_queue_destroy ((GFreeFunc) cairo_destroy, cr);
  sapwood_pixmap_render_rects (pixmap,
                               GTK_TYPE_BUTTON,
                               cr,
                               0, 0,
                               200, 200,
                               mask_surface,
                               0, 0,
                               TRUE,
                               NULL,
                               G_N_ELEMENTS (rects), rects);

  gdk_flush ();
  code = gdk_error_trap_pop ();
  if (code)
    {
      XGetErrorText (GDK_DISPLAY_XDISPLAY (gdk_display_get_default ()),
                     code,
                     abspath,
                     sizeof (abspath)); /* FIXME: check return code once we know _what_ it returns */
      g_warning ("X11 error detected: %s (%d)", abspath, code);
    }

  result = gdk_pixbuf_get_from_drawable (NULL, drawable,
                                         gdk_drawable_get_colormap (drawable),
                                         0, 0, 0, 0,
                                         200, 200);
  g_test_queue_unref (result);

  expected = gdk_pixbuf_new_from_file (TOP_SRCDIR G_DIR_SEPARATOR_S "tests" G_DIR_SEPARATOR_S "test-larger-masked.png", &error);
  g_assert_no_error (error);
  g_test_queue_unref (expected);

  assert_cmp_pixbuf (result, ==, expected);
}

static void
test_larger_masked_offset (void)
{
  cairo_surface_t* mask_surface;
  SapwoodPixmap  * pixmap;
  SapwoodRect      rects[9];
  GdkDrawable    * drawable = NULL;
  GdkPixbuf      * result;
  GdkPixbuf      * expected;
  GdkBitmap      * mask;
  cairo_t        * cr;
  GError         * error = NULL;
  char             abspath[PATH_MAX + 1];
  int              code;
  int              i;

  if (!realpath (TOP_SRCDIR G_DIR_SEPARATOR_S "tests" G_DIR_SEPARATOR_S "test-source-alpha75.png", abspath))
    {
      g_warning ("error in realpath(): \"%s\" causes %s",
                 abspath,
                 g_strerror (errno));
    }
  pixmap = sapwood_pixmap_get_for_file (abspath,
                                        16, 16, 16, 16,
                                        &error);
  g_test_queue_destroy ((GFreeFunc) sapwood_pixmap_free, pixmap);
  g_assert_no_error (error);

  for (i = 0; i < G_N_ELEMENTS (rects); i++)
    {
      int col = i % 3;
      int row = i / 3;

      sapwood_pixmap_get_pixmap (pixmap, col, row,
                                 &rects[i].source, &rects[i].mask);

      rects[i].dest.x = col < 1 ? 0 : col < 2 ? 16 : 200 - 16;
      rects[i].dest.y = row < 1 ? 0 : row < 2 ? 16 : 200 - 16;
      rects[i].dest.width =  col == 1 ? 200 - 2 * 16 : 16;
      rects[i].dest.height = row == 1 ? 200 - 2 * 16 : 16;
    }

  drawable = create_pixmap (200, 200);

  mask = gdk_pixmap_new (NULL, 100, 100, 1);
  g_test_queue_unref (mask);
  mask_surface = sapwood_create_surface (mask);
  g_test_queue_destroy ((GFreeFunc) cairo_surface_destroy, mask_surface);

  gdk_error_trap_push ();

  cr = gdk_cairo_create (drawable);
  g_test_queue_destroy ((GFreeFunc) cairo_destroy, cr);
  sapwood_pixmap_render_rects (pixmap,
                               GTK_TYPE_BUTTON,
                               cr,
                               0, 0,
                               200, 200,
                               mask_surface,
                               -100, -100,
                               TRUE,
                               NULL,
                               G_N_ELEMENTS (rects), rects);

  gdk_flush ();
  code = gdk_error_trap_pop ();
  if (code)
    {
      XGetErrorText (GDK_DISPLAY_XDISPLAY (gdk_display_get_default ()),
                     code,
                     abspath,
                     sizeof (abspath)); /* FIXME: check return code once we know _what_ it returns */
      g_warning ("X11 error detected: %s (%d)", abspath, code);
    }

  result = gdk_pixbuf_get_from_drawable (NULL, drawable,
                                         gdk_drawable_get_colormap (drawable),
                                         100, 100, 0, 0,
                                         100, 100);
  g_test_queue_unref (result);

  expected = gdk_pixbuf_new_from_file (TOP_SRCDIR G_DIR_SEPARATOR_S "tests" G_DIR_SEPARATOR_S "test-larger-masked-offset.png", &error);
  g_assert_no_error (error);
  g_test_queue_unref (expected);

  assert_cmp_pixbuf (result, ==, expected);
}

static void
test_crop (void)
{
  cairo_surface_t* mask_surface;
  SapwoodPixmap  * pixmap;
  SapwoodRect      rects[9];
  GdkRectangle     clip_rect = {0, 0, 50, 50};
  GdkDrawable    * drawable = NULL;
  GdkPixbuf      * result;
  GdkPixbuf      * expected;
  GdkBitmap      * mask;
  cairo_t        * cr;
  GError         * error = NULL;
  char             abspath[PATH_MAX + 1];
  int              code;
  int              i;

  if (!realpath (TOP_SRCDIR G_DIR_SEPARATOR_S "tests" G_DIR_SEPARATOR_S "test-source-alpha75.png", abspath))
    {
      g_warning ("error in realpath(): \"%s\" causes %s",
                 abspath,
                 g_strerror (errno));
    }
  pixmap = sapwood_pixmap_get_for_file (abspath,
                                        16, 16, 16, 16,
                                        &error);
  g_test_queue_destroy ((GFreeFunc) sapwood_pixmap_free, pixmap);
  g_assert_no_error (error);

  for (i = 0; i < G_N_ELEMENTS (rects); i++)
    {
      int col = i % 3;
      int row = i / 3;

      sapwood_pixmap_get_pixmap (pixmap, col, row,
                                 &rects[i].source, &rects[i].mask);

      rects[i].dest.x = col < 1 ? 0 : col < 2 ? 16 : 70 - 16;
      rects[i].dest.y = row < 1 ? 0 : row < 2 ? 16 : 70 - 16;
      rects[i].dest.width =  col == 1 ? 70 - 2 * 16 : 16;
      rects[i].dest.height = row == 1 ? 70 - 2 * 16 : 16;
    }

  drawable = create_pixmap (50, 50);

  mask = gdk_pixmap_new (NULL, 50, 50, 1);
  g_test_queue_unref (mask);
  mask_surface = sapwood_create_surface (mask);
  g_test_queue_destroy ((GFreeFunc) cairo_surface_destroy, mask_surface);

  gdk_error_trap_push ();

  cr = gdk_cairo_create (drawable);
  g_test_queue_destroy ((GFreeFunc) cairo_destroy, cr);
  sapwood_pixmap_render_rects (pixmap,
                               GTK_TYPE_BUTTON,
                               cr,
                               0, 0,
                               50, 50,
                               mask_surface,
                               0, 0,
                               FALSE,
                               &clip_rect,
                               G_N_ELEMENTS (rects), rects);

  gdk_flush ();
  code = gdk_error_trap_pop ();
  if (code)
    {
      XGetErrorText (GDK_DISPLAY_XDISPLAY (gdk_display_get_default ()),
                     code,
                     abspath,
                     sizeof (abspath)); /* FIXME: check return code once we know _what_ it returns */
      g_warning ("X11 error detected: %s (%d)", abspath, code);
    }

  result = gdk_pixbuf_get_from_drawable (NULL, drawable,
                                         gdk_drawable_get_colormap (drawable),
                                         0, 0, 0, 0,
                                         50, 50);
  g_test_queue_unref (result);

  expected = gdk_pixbuf_new_from_file (TOP_SRCDIR G_DIR_SEPARATOR_S "tests" G_DIR_SEPARATOR_S "theme-pixbuf-render-cropped.png", &error);
  g_assert_no_error (error);
  g_test_queue_unref (expected);

  assert_cmp_pixbuf (result, ==, expected);
}

void
test_sapwood_pixmap (void)
{
  g_test_add_func ("/sapwood/pixmap/render-rects/larger", test_larger);
  g_test_add_func ("/sapwood/pixmap/render-rects/larger-masked", test_larger_masked);
  g_test_add_func ("/sapwood/pixmap/render-rects/larger-masked-offset", test_larger_masked_offset);
  // FIXME: add: g_test_add_func ("/sapwood/pixmap/render-rects/crop-x", test_crop_x);
  // FIXME: add: g_test_add_func ("/sapwood/pixmap/render-rects/crop-y", test_crop_y);
  g_test_add_func ("/sapwood/pixmap/render-rects/crop-xy", test_crop);
}

/* vim:set et sw=2 cino=t0,f0,(0,{s,>2s,n-1s,^-1s,e2s: */
