#!/bin/sh

set -e
set -x
libtoolize --automake
aclocal || aclocal-1.8
autoconf
autoheader
automake --add-missing --foreign || automake-1.8 --add-missing --foreign

if test x$NOCONFIGURE = x; then
  ./configure --enable-maintainer-mode --enable-debug "$@"
else
  echo Skipping configure process.
fi
