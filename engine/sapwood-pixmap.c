/* GTK+ Sapwood Engine
 * Copyright (C) 2005 Nokia Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Written by Tommi Komulainen <tommi.komulainen@nokia.com>
 */
#include <config.h>

#include "sapwood-cairo.h"
#include "sapwood-debug.h"
#include "sapwood-pixmap-priv.h"
#include "sapwood-proto.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

gboolean sapwood_debug_scaling = FALSE;
gboolean sapwood_debug_xtraps = FALSE;

static gboolean
pixbuf_proto_request (const char *req,
                      ssize_t     reqlen,
                      char       *rep,
                      ssize_t     replen,
                      GError    **err)
{
  static int fd = -1;
  ssize_t    n;

  if (fd == -1)
    {
      fd = sapwood_client_get_socket (err);
      if (fd == -1)
	return FALSE;
    }

  n = write (fd, req, reqlen);
  if (n < 0)
    {
      g_set_error (err, SAPWOOD_CLIENT_ERROR, SAPWOOD_CLIENT_ERROR_UNKNOWN,
		   "write: %s", g_strerror (errno));
      return FALSE;
    }
  else if (n != reqlen)
    {
      /* FIXME */
      g_set_error (err, SAPWOOD_CLIENT_ERROR, SAPWOOD_CLIENT_ERROR_UNKNOWN,
		   "wrote %d of %d bytes", n, reqlen);
      return FALSE;
    }

  if (!rep)
    return TRUE;

  n = read (fd, rep, replen);
  if (n < 0)
    {
      g_set_error (err, SAPWOOD_CLIENT_ERROR, SAPWOOD_CLIENT_ERROR_UNKNOWN,
		   "read: %s", g_strerror (errno));
      return FALSE;
    }
  else if (n != replen)
    {
      /* FIXME */
      g_set_error (err, SAPWOOD_CLIENT_ERROR, SAPWOOD_CLIENT_ERROR_UNKNOWN,
		   "read %d, expected %d bytes", n, replen);
      return FALSE;
    }

  return TRUE;
}

SapwoodPixmap *
sapwood_pixmap_get_for_file (const char *filename,
                             int         border_left,
                             int         border_right,
                             int         border_top,
                             int         border_bottom,
                             GError    **err)
{
  SapwoodPixmap     *self;
  char               buf[ sizeof(PixbufOpenRequest) + PATH_MAX + 1 ] = {0};
  PixbufOpenRequest *req = (PixbufOpenRequest *) buf;
  PixbufOpenResponse rep;
  int                flen;
  int                i, j;

  /* marshal request */
  flen = g_strlcpy (req->filename, filename, PATH_MAX);
  if (flen > PATH_MAX)
    {
      g_set_error (err, SAPWOOD_CLIENT_ERROR, SAPWOOD_CLIENT_ERROR_UNKNOWN,
		   "%s: filename too long", filename);
      return NULL;
    }

  req->base.op       = PIXBUF_OP_OPEN;
  req->base.length   = sizeof(*req) + flen + 1;
  req->border_left   = border_left;
  req->border_right  = border_right;
  req->border_top    = border_top;
  req->border_bottom = border_bottom;

  if (!pixbuf_proto_request ((char*)req,  req->base.length,
			     (char*)&rep, sizeof(rep), err))
    return NULL;

  /* unmarshal response */
  self = g_new0 (SapwoodPixmap, 1);
  self->id     = rep.id;
  self->width  = rep.width;
  self->height = rep.height;

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
        cairo_surface_t* mask = NULL;
	GdkPixmap      * pixmap  = NULL;
	int xerror;

	if (rep.pixmap[i][j])
	  {
	    gdk_error_trap_push ();
            pixmap = gdk_pixmap_foreign_new (rep.pixmap[i][j]);
            gdk_drawable_set_colormap (pixmap, gdk_screen_get_system_colormap (gdk_screen_get_default ()));

            if (sapwood_debug_xtraps)
              gdk_flush ();

	    if ((xerror = gdk_error_trap_pop ()) || !pixmap)
	      {
		g_warning ("%s: pixmap[%d][%d]: gdk_pixmap_foreign_new(%x) failed, X error = %d",
			   g_basename (filename), i, j, rep.pixmap[i][j], xerror);
		if (pixmap)
		  g_object_unref (pixmap);
		pixmap = NULL;
	      }
	  }

	if (rep.pixmask[i][j])
	  {
            GdkBitmap* pixmask;

	    gdk_error_trap_push ();

            pixmask = gdk_pixmap_foreign_new (rep.pixmask[i][j]);

            if (sapwood_debug_xtraps)
              gdk_flush ();

	    if ((xerror = gdk_error_trap_pop ()) || !pixmask)
	      {
                g_warning ("%s: pixmask[%d][%d]: gdk_pixmap_foreign_new(%x) failed, X error = %d",
			   g_basename (filename), i, j, rep.pixmask[i][j], xerror);
		if (pixmask)
		  g_object_unref (pixmask);
	      }
            else
              {
                mask = sapwood_create_surface_and_unref (pixmask);
              }
	  }

	if (mask && !pixmap)
	  {
	    g_warning ("%s: pixmask[%d][%d]: no pixmap", g_basename (filename), i, j);
	  }

        self->sources[i][j] = sapwood_create_surface_and_unref (pixmap);
        self->masks[i][j]   = mask;
      }

  return self;
}

static void
pixbuf_proto_unref_pixmap (guint32 id)
{
  PixbufCloseRequest  req;
  GError             *err = NULL;

  LOG ("%s(): unref %d", G_STRFUNC, id);

  req.base.op     = PIXBUF_OP_CLOSE;
  req.base.length = sizeof(PixbufCloseRequest);
  req.id          = id;
  if (!pixbuf_proto_request ((char*)&req, req.base.length, NULL, 0, &err))
    {
      g_warning ("close(0x%x): %s", id, err->message);
      g_error_free (err);
      return;
    }
}

void
sapwood_pixmap_free (SapwoodPixmap *self)
{
  int         i, j;

  g_return_if_fail (self);

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      if (self->sources[i][j])
        {
          cairo_surface_destroy (self->sources[i][j]);
          if (self->masks[i][j])
            cairo_surface_destroy (self->masks[i][j]);
        }

  /* need to make sure all our operations are processed before the pixmaps
   * are free'd by the server or we risk causing BadPixmap error */
  gdk_display_sync (gdk_display_get_default ());

  pixbuf_proto_unref_pixmap (self->id);
  g_free (self); /* FIXME: use g_slice() at least */
}

gboolean
sapwood_pixmap_get_geometry (SapwoodPixmap *self,
                             gint          *width,
                             gint          *height)
{
  if (!self)
    return FALSE;

  if (width) 
    *width = self->width;
  if (height)
    *height = self->height;

  return TRUE;
}

void
sapwood_pixmap_get_pixmap (SapwoodPixmap  * self,
                           gint             x,
                           gint             y,
                           cairo_surface_t**out_source,
                           cairo_surface_t**out_mask)
{
  *out_source = self->sources[y][x];
  *out_mask   = self->masks[y][x];
}

/*
 * sapwood_pixmap_render_rects_internal:
 * @self: a #SapwoodPixmap
 * @cr: the #cairo_t context to draw to
 * @draw_x: the horizontal offset on @drawable in pixels
 * @draw_y: the vertical offset on @drawable in pixels
 * @mask: a #cairo_surface_t mask to paint @self's bitmask to and to use when the 1-bit transparency should be honored
 * @mask_x: the horizontal offset on @mask in pixels
 * @mask_y: the vertical offset on @mask in pixels
 * @mask_required: FIXME
 * @clip_rect: the clipping area of the rendering
 * @n_rect: the number of rectangles in @rect
 * @rect: an array of rectangles to render
 *
 * Render the areas of @self specified by @rect to @drawable.
 */
static void
sapwood_pixmap_render_rects_internal (SapwoodPixmap  * self,
                                      cairo_t        * cr,
                                      gint             draw_x,
                                      gint             draw_y,
                                      cairo_surface_t* mask,
                                      gint             mask_x,
                                      gint             mask_y,
                                      gboolean         mask_required,
                                      GdkRectangle   * clip_rect,
                                      gint             n_rect,
                                      SapwoodRect    * rect)
{
  gint          xofs;
  gint          yofs;
  gint          n;
  gboolean      have_mask = FALSE;

  xofs = draw_x - mask_x;
  yofs = draw_y - mask_y;

  /* FIXME: try to rewrite this to use one loop only */

  if (mask)
    {
      cairo_t* mask_cr = cairo_create (mask);
      cairo_set_operator (mask_cr, CAIRO_OPERATOR_SOURCE);
      cairo_translate (mask_cr, -xofs, -yofs);

      for (n = 0; n < n_rect; n++)
	{
	  /* const */ GdkRectangle *dest = &rect[n].dest;
	  GdkRectangle              area;

	  if (!mask_required && clip_rect)
	    {
	      if (!gdk_rectangle_intersect (dest, clip_rect, &area))
		continue;
	    }
	  else
	    area = *dest;

	  if (rect[n].source && rect[n].mask)
	    {
              cairo_set_source_surface (mask_cr, rect[n].mask, area.x, area.y);
              cairo_pattern_set_extend (cairo_get_source (mask_cr), CAIRO_EXTEND_REPEAT);
              gdk_cairo_rectangle (mask_cr, &area);
              cairo_fill (mask_cr);

	      have_mask = TRUE;
	    }
	}
      cairo_destroy (mask_cr);
    }

  for (n = 0; n < n_rect; n++)
    {
      /* const */ GdkRectangle *dest = &rect[n].dest;
      GdkRectangle              area;

      if (clip_rect)
	{
	  if (!gdk_rectangle_intersect (dest, clip_rect, &area))
	    continue;
	}
      else
	area = *dest;

      if (rect[n].source)
	{
          cairo_save (cr);

          cairo_set_source_surface (cr, rect[n].source, dest->x, dest->y);
          cairo_pattern_set_extend (cairo_get_source (cr), CAIRO_EXTEND_REPEAT);

          gdk_cairo_rectangle (cr, &area);
          cairo_clip (cr);
          if (mask)
            {
#if 0
              cairo_pattern_t* pattern;
              cairo_matrix_t   matrix;

              cairo_matrix_init_identity (&matrix);
              cairo_matrix_init_translate (&matrix, -xofs, -yofs);
              pattern = cairo_pattern_create_for_surface (mask);
              cairo_pattern_set_matrix (pattern, &matrix);
              cairo_mask (cr, pattern);
              cairo_pattern_destroy (pattern);
#else
              cairo_mask_surface (cr, mask, xofs, yofs);
#endif
            }
          else
            {
              cairo_paint (cr);
            }

          cairo_restore (cr);
	}
    }
}

static void
sapwood_crop_single_dimension (cairo_surface_t* surface,
                               int              x,
                               int              y,
                               int              width,
                               int              height,
                               int              displace_x,
                               int              displace_y)
{
  cairo_t* cr = cairo_create (surface);
  cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
  cairo_set_source_surface (cr, surface, displace_x, displace_y);
  cairo_rectangle (cr, x, y, width, height);
  cairo_fill (cr);
  cairo_destroy (cr);
}

static void
sapwood_crop_single_surface (cairo_surface_t* surface,
                             int              requested_width,
                             int              requested_height,
                             int              original_width,
                             int              original_height)
{
  cairo_t* cr = cairo_create (surface);

  if (requested_width < original_width)
    {
      int      left  = requested_width / 2;
      int      right = requested_width - left;

      sapwood_crop_single_dimension (surface,
                                   left, 0,
                                   right, MAX (original_height, requested_height),
                                   requested_width - original_width, 0);
    }

  if (requested_height < original_height)
    {
      int top    = requested_height / 2;
      int bottom = requested_height - top;

      sapwood_crop_single_dimension (surface,
                                   0, top,
                                   MAX (original_width, requested_width), bottom,
                                   0, requested_height - original_height);
    }

  cairo_destroy (cr);
}

static void /* FIXME: consider renaming to sapwood_crop_surfaces() */
sapwood_crop_pixmap (cairo_surface_t* surface,
                     cairo_surface_t* mask,
                     int              requested_width,
                     int              requested_height,
                     int              original_width,
                     int              original_height)
{
  sapwood_crop_single_surface (surface,
                               requested_width, requested_height,
                               original_width, original_height);

  if (mask)
    {
      sapwood_crop_single_surface (mask,
                                   requested_width, requested_height,
                                   original_width, original_height);
    }
}

void
sapwood_pixmap_render_rects (SapwoodPixmap* self,
                             GType          widget_type,
                             cairo_t      * cr,
                             gint           draw_x,
                             gint           draw_y,
                             gint           width,
                             gint           height,
                             cairo_surface_t     *mask,
                             gint           mask_x,
                             gint           mask_y,
                             gboolean       mask_required,
                             GdkRectangle  *clip_rect,
                             gint           n_rect,
                             SapwoodRect   *rect)
{
  cairo_surface_t* mask_surface = NULL;
  cairo_surface_t* tmp;
  gboolean         need_tmp_mask = FALSE;
  cairo_t        * tmp_cr;
  gint             n;
  gint             tmp_width;
  gint             tmp_height;

  /* Don't even try to scale down shape masks (should never be useful, and
   * implementing would add some complexity.) Areas larger than the pixmap
   * can be tiled fine.
   */
  if (mask_required || (width >= self->width && height >= self->height))
    {
      sapwood_pixmap_render_rects_internal (self, cr, draw_x, draw_y, mask, mask_x, mask_y, mask_required, clip_rect, n_rect, rect);
      return;
    }

  /* offset the drawing on temporary pixmap */
  for (n = 0; n < n_rect; n++)
    {
      SapwoodRect *r = &rect[n];
      r->dest.x -= draw_x;
      r->dest.y -= draw_y;
      if (r->source && r->mask)
	need_tmp_mask = TRUE;
    }

  /* prepare temporary pixmap and mask to draw in full size */
  tmp_width = MAX(width, self->width);
  tmp_height = MAX(height, self->height);

  tmp = cairo_surface_create_similar (cairo_get_target (cr),
                                      cairo_surface_get_content (cairo_get_target (cr)),
                                      tmp_width, tmp_height);

  if (need_tmp_mask)
    {
      GdkPixmap* tmp_mask = gdk_pixmap_new (NULL, tmp_width, tmp_height, 1);
      mask_surface = sapwood_create_surface_and_unref (tmp_mask);
    }

  tmp_cr = cairo_create (tmp);
  sapwood_pixmap_render_rects_internal (self, tmp_cr, 0, 0, mask_surface, 0, 0, mask_required, NULL, n_rect, rect);

  cairo_save (cr);
  /* finally, render downscaled at draw_x,draw_y */
  if (clip_rect)
    {
      gdk_cairo_rectangle (cr, clip_rect);
      cairo_clip (cr);
    }
  cairo_rectangle (cr, draw_x, draw_y, width, height);
  cairo_clip (cr);

  cairo_translate (cr, draw_x, draw_y);

  if ((width > 0 && width < tmp_width) || (height > 0 && height < tmp_height))
    {
      sapwood_crop_pixmap (tmp, mask_surface, width, height,
			   self->width, self->height);

      cairo_rectangle (cr, 0, 0, width, height);
      cairo_clip (cr);
    }
  else if (width != tmp_width || height != tmp_height)
    {
      /* this code is currently not reached, we should enable it with a sapwood-specific
       * shrink-behavior property (and maybe add simple clipping along with it) */
      cairo_scale (cr, (double)width / (double)tmp_width,
		       (double)height / (double)tmp_height);

      if (sapwood_debug_scaling)
        {
	  g_warning ("scaling pixmap for %s: requested %dx%d; real %dx%d",
		     g_type_name (widget_type),
		     width, height,
		     self->width, self->height);
	  cairo_save (tmp_cr);
	  cairo_set_source_rgba (tmp_cr, 1.0, 1.0, 0.0, 0.25);
	  cairo_paint (tmp_cr);
	  cairo_restore (tmp_cr);
        }
    }

  cairo_set_source_surface (cr, cairo_get_target (tmp_cr), 0, 0);
  if (mask_surface)
    cairo_mask_surface (cr, mask_surface, 0, 0);
  else
    cairo_paint (cr);

  /* clean up */
  cairo_restore (cr);
  if (mask_surface)
    {
      cairo_surface_destroy (mask_surface);
    }
  cairo_destroy (tmp_cr);
  cairo_surface_destroy (tmp);
}

/* vim:set et sw=2 cino=t0,f0,(0,{s,>2s,n-1s,^-1s,e2s: */
