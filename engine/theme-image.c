/* This file is part of the sapwood theming engine for GTK+
 *
 * Copyright (C) 1998-2000  Red Hat, Inc.
 * Copyright (C) 2005,2007  Nokia Corporation
 * Copyright (C) 2010  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "theme-image.h"

#include <string.h>
#include "sapwood-rc-style.h"

G_DEFINE_TYPE (ThemeImage, theme_image, G_TYPE_OBJECT);

static void
theme_image_init (ThemeImage* self)
{}

static void
finalize (GObject* object)
{
  ThemeImage* data = THEME_IMAGE (object);

  g_free (data->match_data.detail);
  if (data->background)
    theme_pixbuf_unref (data->background);
  if (data->overlay)
    theme_pixbuf_unref (data->overlay);
  if (data->gap_start)
    theme_pixbuf_unref (data->gap_start);
  if (data->gap)
    theme_pixbuf_unref (data->gap);
  if (data->gap_end)
    theme_pixbuf_unref (data->gap_end);

  G_OBJECT_CLASS (theme_image_parent_class)->finalize (object);
}

static void
theme_image_class_init (ThemeImageClass* self_class)
{
  GObjectClass* object_class = G_OBJECT_CLASS (self_class);

  object_class->finalize = finalize;
}

ThemeImage *
match_theme_image (GtkStyle       *style,
		   ThemeMatchData *match_data)
{
  GList *tmp_list;

  tmp_list = SAPWOOD_RC_STYLE (style->rc_style)->img_list;

  while (tmp_list)
    {
      guint flags;
      ThemeImage *image = tmp_list->data;
      tmp_list = tmp_list->next;

      if (match_data->function != image->match_data.function)
	continue;

      flags = match_data->flags & image->match_data.flags;

      if (flags != image->match_data.flags) /* Required components not present */
	continue;

      if ((flags & THEME_MATCH_STATE) &&
	  match_data->state != image->match_data.state)
	continue;

      if ((flags & THEME_MATCH_POSITION) &&
	  match_data->position != image->match_data.position)
	continue;

      if ((flags & THEME_MATCH_SHADOW) &&
	  match_data->shadow != image->match_data.shadow)
	continue;

      if ((flags & THEME_MATCH_ARROW_DIRECTION) &&
	  match_data->arrow_direction != image->match_data.arrow_direction)
	continue;

      if ((flags & THEME_MATCH_ORIENTATION) &&
	  match_data->orientation != image->match_data.orientation)
	continue;

      if ((flags & THEME_MATCH_GAP_SIDE) &&
	  match_data->gap_side != image->match_data.gap_side)
	continue;

      /* simple pattern matching for (treeview) details
       * in gtkrc 'detail = "*_start"' will match all calls with detail ending
       * with '_start' such as 'cell_even_start', 'cell_odd_start', etc.
       */
      if (image->match_data.detail)
        {
          if (!match_data->detail)
            continue;
          else if (image->match_data.detail[0] == '*')
            {
              if (!g_str_has_suffix (match_data->detail, image->match_data.detail + 1))
                continue;
            }
          else if (strcmp (match_data->detail, image->match_data.detail) != 0)
            continue;
        }

      return image;
    }

  return NULL;
}

ThemeImage*
theme_image_new (void)
{
  return g_object_new (THEME_TYPE_IMAGE,
                       NULL);
}

/* vim:set et sw=2 cino=t0,f0,(0,{s,>2s,n-1s,^-1s,e2s: */
