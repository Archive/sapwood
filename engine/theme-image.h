/* This file is part of the sapwood theming engine for GTK+
 *
 * Copyright (C) 1998-2000  Red Hat, Inc.
 * Copyright (C) 2005,2007  Nokia Corporation
 * Copyright (C) 2010  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef THEME_IMAGE_H
#define THEME_IMAGE_H

#include "theme-pixbuf.h"

G_BEGIN_DECLS

typedef struct _ThemeImage      ThemeImage;
typedef struct _ThemeImageClass ThemeImageClass;
typedef struct _ThemeMatchData  ThemeMatchData;

#define THEME_TYPE_IMAGE         (theme_image_get_type ())
#define THEME_IMAGE(i)           (G_TYPE_CHECK_INSTANCE_CAST ((i), THEME_TYPE_IMAGE, ThemeImage))
#define THEME_IMAGE_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST    ((c), THEME_TYPE_IMAGE, ThemeImageClass))
#define THEME_IS_IMAGE(i)        (G_TYPE_CHECK_INSTANCE_TYPE ((i), THEME_TYPE_IMAGE))
#define THEME_IS_IMAGE_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE    ((c), THEME_TYPE_IMAGE))
#define THEME_IMAGE_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS  ((i), THEME_TYPE_IMAGE, ThemeImageClass))

GType       theme_image_get_type (void) G_GNUC_INTERNAL;

ThemeImage* match_theme_image    (GtkStyle      * style,
                                  ThemeMatchData* match_data) G_GNUC_INTERNAL;

ThemeImage* theme_image_new      (void) G_GNUC_INTERNAL;


typedef enum {
  THEME_MATCH_GAP_SIDE        = 1 << 0,
  THEME_MATCH_ORIENTATION     = 1 << 1,
  THEME_MATCH_STATE           = 1 << 2,
  THEME_MATCH_SHADOW          = 1 << 3,
  THEME_MATCH_ARROW_DIRECTION = 1 << 4,
  THEME_MATCH_POSITION        = 1 << 5
} ThemeMatchFlags;

typedef enum {
  THEME_POS_LEFT   = 1 << 0, /* GTK_POS_LEFT   */
  THEME_POS_RIGHT  = 1 << 1, /* GTK_POS_RIGHT  */
  THEME_POS_TOP    = 1 << 2, /* GTK_POS_TOP    */
  THEME_POS_BOTTOM = 1 << 3  /* GTK_POS_BOTTOM */
} ThemePositionFlags;

struct _ThemeMatchData
{
  gchar          *detail;
  guint16         function;	/* Mandatory */

  ThemeMatchFlags flags           : 6;
  ThemePositionFlags position     : 4;
  GtkStateType    state           : 3;
  GtkShadowType   shadow          : 3;
  GtkPositionType gap_side        : 2;
  guint           arrow_direction : 2;	/* GtkArrowType, but without NONE */
  GtkOrientation  orientation     : 1;
};

struct _ThemeImage
{
  GObject         base_instance;
  ThemePixbuf    *background;
  ThemePixbuf    *overlay;
  ThemePixbuf    *gap_start;
  ThemePixbuf    *gap;
  ThemePixbuf    *gap_end;

  ThemeMatchData  match_data;

  guint           background_shaped : 1;
};

struct _ThemeImageClass
{
  GObjectClass    base_class;
};

G_END_DECLS

#endif /* !THEME_IMAGE_H */

/* vim:set et sw=2 cino=t0,f0,(0,{s,>2s,n-1s,^-1s,e2s: */
