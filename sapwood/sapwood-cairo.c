/* This file is part of sapwood
 *
 * Copyright (C) 2010  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "sapwood-cairo.h"

static cairo_user_data_key_t  sapwood_surface_user_data;

cairo_surface_t*
sapwood_create_surface (GdkDrawable* drawable)
{
  cairo_surface_t* result;
  cairo_t        * cr;

  cr = gdk_cairo_create (drawable);
  result = cairo_get_target (cr);
  cairo_surface_reference (result);
  cairo_destroy (cr);

  return result;
}

cairo_surface_t*
sapwood_create_surface_and_unref (GdkDrawable* drawable)
{
  cairo_surface_t* result = sapwood_create_surface (drawable);

  cairo_surface_set_user_data (result, &sapwood_surface_user_data,
                               drawable, g_object_unref);

  return result;
}
/* vim:set et sw=2 cino=t0,f0,(0,{s,>2s,n-1s,^-1s,e2s: */
