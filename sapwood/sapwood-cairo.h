/* This file is part of sapwood
 *
 * Copyright (C) 2010  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef SAPWOOD_CAIRO_H
#define SAPWOOD_CAIRO_H

#include <cairo.h>
#include <gdk/gdk.h>

G_BEGIN_DECLS

cairo_surface_t* sapwood_create_surface           (GdkDrawable* drawable) G_GNUC_INTERNAL;
cairo_surface_t* sapwood_create_surface_and_unref (GdkDrawable* drawable) G_GNUC_INTERNAL;

G_END_DECLS

#endif /* !SAPWOOD_CAIRO_H */

/* vim:set et sw=2 cino=t0,f0,(0,{s,>2s,n-1s,^-1s,e2s: */
