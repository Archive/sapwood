# vim:set ft=automake:

AM_CPPFLAGS=$(WARN_CFLAGS)

noinst_LTLIBRARIES+=libsapwood.la
libsapwood_la_CPPFLAGS=\
	-DG_LOG_DOMAIN=\""libsapwood"\" \
	$(AM_CPPFLAGS) \
	$(GDK_CFLAGS) \
	$(NULL)
libsapwood_la_LIBADD=\
	$(LIBADD) \
	$(GDK_LIBS) \
	$(NULL)
libsapwood_la_SOURCES=\
	sapwood/sapwood-cairo.c \
	sapwood/sapwood-cairo.h \
	$(NULL)
